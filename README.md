# [The Flying Knight - Teasbags Animal Patch](https://www.nexusmods.com/skyrimspecialedition/mods/27246)

## Bugs

If you find an issue, let us know [here](https://gitlab.com/lexys-lotd-se/the-flying-knight-teasbags-animal-patch/issues)!
